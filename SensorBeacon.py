
from machine import Pin, I2C, RTC
import BME280
import time
import json

class SensorBeacon():

    def __init__(self):
        # led config
        self.ledpin = Pin(2, Pin.OUT)
        self.ledpin.on()

        # system config
        with open ("systemconfiguration.json", "r") as f:
            self.config = json.loads(f.read())

        # sensor config
        self.i2c = I2C(scl=Pin(5), sda=Pin(4), freq=10000)

        # deepsleep config
        self.rtc = RTC()

    def deepsleep(self, seconds=10):
        import machine

        # configure RTC.ALARM0 to be able to wake the device
        self.rtc.irq(trigger=self.rtc.ALARM0, wake=machine.DEEPSLEEP)

        # set RTC.ALARM0 to fire after 10 seconds (waking the device)
        self.rtc.alarm(self.rtc.ALARM0, seconds*1000)

        # put the device to sleep
        print("Falling to deepsleep now with %d second wakeup timeout" % seconds)
        machine.deepsleep()

    def sensor_read(self):
        import math
        bme = BME280.BME280(i2c=self.i2c)
        t = bme.temperature
        h = bme.humidity
        p = bme.pressure
        Pvh = h/100*math.e**(77.345+0.0057*(t+273.15)-7235/(t+273.15))/((t+273.15)**8.2)
        Pkok = p*100.0
        d = {
            't': round(t, 2),   # temperature, celsius
            'h': round(h, 2),   # humidity, %
            'p': round(p, 2),   # pressure, hPa
            'dp': round(17.27*t/(237.7+t)+math.log(h/100.0), 4), # dewpoint, celsius
            'ah': round(1000.0*18.016/28.964*Pvh/(Pkok-Pvh), 4)  # absosute humidity g/kg[h2o]
        }
        return d

    def read_once_local_test(self):
        fn = 'readings.txt'
        r = self.sensor_read()
        with open (fn, 'a+') as f:
            f.write(json.dumps(r)+'\n')
            print ("Wrote: %s" % json.dumps(r))
        self.deepsleep(seconds=15*60)

    def wlan_connect(self):
        import network
        sta_if = network.WLAN(network.STA_IF)
        if not sta_if.isconnected():
            print('connecting to network...')
            sta_if.active(True)
            sta_if.connect(self.config['wlanssid'], self.config['wlanpassword'])
            while not sta_if.isconnected():
                time.sleep(1)
                print("Waiting WLAN...")
        print('network config:', sta_if.ifconfig())

    def mqtt(self):

        import json
        from umqtt.robust import MQTTClient
        
        try:
            if 1: # This path enables communication to Azure:
                with open(self.config['azurecertificateDER'],'rb') as f:  cert = f.read()
                with open(self.config['azureprivatekeyDER'],'rb') as f:   key = f.read()
                server = self.config['azureiothub']
                device = self.config['azuredevice']
                topic = "devices/%s/messages/events/" % device

            else: # This path does the same for AWS:
                with open(self.config['awscertificateDER'],'rb') as f:  cert = f.read()
                with open(self.config['awsprivatekeyDER'],'rb') as f:   key = f.read()
                server = self.config['awsiothub']
                device = self.config['awsdevice']
                topic = "telemetry"

            # Read current sensor values:
            sensor_data = self.sensor_read()
            print("Sensor data: ", sensor_data)

            # Make sure we have 'internet'
            self.wlan_connect()

            # Create MQTT client:
            self.mqtt_client = MQTTClient(
                client_id=device, 
                server=server,
                port=8883, 
                keepalive=5000, 
                ssl=True,
                user = "%s/%s" % (server, device),  # User and password are needed by Azure, but not by AWS
                password = "",                      # AWS ignores those, so let's just keep 'em here.'
                ssl_params={"cert":cert, "key":key})

            print("Trying connection to %s IoT hub..." % server)
            self.mqtt_client.connect()

            print('Hub connection established, publishing msg to "%s" ... ' % topic)
            self.mqtt_client.publish(topic, json.dumps(sensor_data), retain=False, qos=1)
            print("Publish successful")
            
        except Exception as e:
            print('Cannot connect MQTT: ' + str(e))
            raise
