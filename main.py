import machine
import time

# hereby, typing mem() will tell you the free ram
def mem():
	import gc
	gc.collect()
	print(gc.mem_free())

# for testing, wait for serial connection. This gives a delay so that
# one can attach usb cable and start picocom
# (and optionally press ctlr-c before the execution continues...)
print ("Waiting politely for serial connection (for debug)")
time.sleep(5)

from SensorBeacon import SensorBeacon

# When we wake up, we want to connecto WLAN, read sensors (and publish the readings)
# and then go back to deepsleep

S = SensorBeacon()
S.mqtt()
S.deepsleep(seconds=15)
