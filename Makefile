
PORT=/dev/ttyUSB0

default:
	@echo "Available make targets:"
	@echo "BME280:        Download BME280 python driver"
	@echo "upload-code:   Upload all python files into NodeMCU device"
	@echo "upload-keys:   Upload all configurations and keys into NodeMCU device"
	@echo "virtualenv:    Install new python virtual environment"
	@echo "serialconnect: Make python shell connection to the NodeMCU device at port ${PORT}"
	@echo "listdir:       Show directory contents of / of NodeMCU"
	@echo "getreadings:   Get locally stored sensor readings (if any)"
	@echo "zeroreadings:  Delete existing local sensor readings (if any)"

BME280:
	-wget wget http://acoptex.com/uploads/pro1_BME280.py -O BME280.py

upload-code:
	ampy -p ${PORT} -b 115200 put main.py
	ampy -p ${PORT} -b 115200 put SensorBeacon.py
	ampy -p ${PORT} -b 115200 put BME280.py

upload-keys:
	ampy -p ${PORT} -b 115200 put systemconfiguration.json
	ampy -p ${PORT} -b 115200 put cert-aws.der
	ampy -p ${PORT} -b 115200 put key-aws.der
	ampy -p ${PORT} -b 115200 put cert-azure.der
	ampy -p ${PORT} -b 115200 put key-azure.der

listdir:
	ampy -p ${PORT} -b 115200 ls

getreadings:
	ampy -p ${PORT} -b 115200 get readings.txt

zeroreadings:
	rm -rf readinds.txt
	touch readings.txt
	ampy -p ${PORT} -b 115200 put readings.txt

virtualenv:
	@if [ ! -d env ]; then \
	  echo "Installing new virtualenv in env"; \
	  python3 -m venv env; \
	  echo "Virtual env installed in env. Now activate it and install dependencies:"; \
	  echo " $ source env/bin/activate"; \
	  echo " $ pip install -r requirements.txt"; \
	else \
	  echo "Virtualenv already installed in env. Use:"; \
	  echo " $ source env/bin/activate"; \
	fi

serialconnect:
	picocom --b 115200 ${PORT}
