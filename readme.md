
# Sensor beacon based on NodeMCU v3 (esp8266 microcontroller) using micropython

A firmware project for NodeMCU v3 using micropython. It is a beacon that utilizes available WLAN to connect to internet, BME280 sensor to read temperature, humidity and airpressure levels and hardware deepsleep features to minimize battery consumption. It is a beacon which submits read sensor values at configurable interval to the configured internet connection. AWS and Azure Iot connection is presented as an example.

## useful links:

- Hardware informtion of NodeMCU v3, pin layouts etc. nice stuff https://www.theengineeringprojects.com/2018/10/introduction-to-nodemcu-v3.html
- How to run micropython on esp8266 https://docs.micropython.org/en/latest/esp8266/tutorial/intro.html

## How to ...

The application in this repo is a full app, in other words: if you have micropython on your esp8266 then this code is directly in run condition. Just submit the WLAN credentials and/or aws/azure credentials to enable the communication. These, however, are only optional. The sensor data can be read and stored locally if the network connection is not available.

Check out the Makefile in the project, it gathers all the important commands as build targets to ease the work. 

Happy hacking.
